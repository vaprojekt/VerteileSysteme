## Verteilte Systeme Projekt: Fahrplanner

Es handelt sich um eine Java-Web-Applikation, die z.B. auf dem Apache Tomcat gestartet werden kann. Als Build-System wird *Maven* verwendet. 

Die vorliegende Version adressiert das `jakarta.jakartaee-web-api`. Der Web-Server (z.B. Tomcat 11) sollte dieses API unterstützen. Die notwendigen Dependencies sind in der Datei `pom.xml` konfiguriert.

<sub>do, nguyen / Februar 2024<sub>

#Compile maven
```
mvn clean
mvn install
```

#Start WebApp
```
mvn verify org.codehaus.cargo:cargo-maven3-plugin:run -Dcargo.maven.containerId=tomcat10x -Dcargo.maven.containerUrl=https://repo.maven.apache.org/maven2/org/apache/tomcat/tomcat/10.1.8/tomcat-10.1.8.zip
```

#Altenativ
```
./run.bash
```

In Browser, go to URL:
```
http://server_ip:8080/
```