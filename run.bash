#!/usr/bin/bash

#Compile maven
mvn clean
mvn install


#Start WebApp
mvn verify org.codehaus.cargo:cargo-maven3-plugin:run -Dcargo.maven.containerId=tomcat10x -Dcargo.maven.containerUrl=https://repo.maven.apache.org/maven2/org/apache/tomcat/tomcat/10.1.8/tomcat-10.1.8.zip
