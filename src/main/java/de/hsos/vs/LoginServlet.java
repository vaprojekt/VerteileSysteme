package de.hsos.vs;
import de.hsos.vs.service.AuthenticationService;
import de.hsos.vs.service.DebugUtil;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import jakarta.servlet.http.Cookie;

import java.io.IOException;
import java.io.PrintWriter;


@WebServlet(name = "loginServlet", urlPatterns = {"/login"})
public class LoginServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {

        HttpSession session = req.getSession();
        String user = req.getParameter("theUser");
        String password = req.getParameter("thePassword");

        // Authenticate user
        boolean authenticated = AuthenticationService.authenticateUser(user, password);
        DebugUtil.log("Authenticated is: " + authenticated);
        if (authenticated) {

            session.setAttribute("user", user);
            //setting session to expiry in 30 mins
            session.setMaxInactiveInterval(30*60);
            Cookie userName = new Cookie("user", user);
            resp.addCookie(userName);
            //Get the encoded URL string
            String encodedURL = resp.encodeRedirectURL("routefinder.jsp");
            resp.sendRedirect(encodedURL);
            DebugUtil.log("Login successful");
        } else {
            forwardToHomePage(req, resp, "Login failed. Invalid user credentials");
        }
    }

    public static void forwardToHomePage(HttpServletRequest req, HttpServletResponse resp,
                                      String errorMessage)
            throws ServletException, IOException {

        PrintWriter out= resp.getWriter();
        out.println("<font color=red>" + errorMessage + "</font>"); //Either user name or password is wrong.
        req.getRequestDispatcher("/").include(req, resp);
        DebugUtil.log("Forwarded to homepage");
    }
}