package de.hsos.vs;

import de.hsos.vs.entity.User;
import de.hsos.vs.service.AuthenticationService;
import de.hsos.vs.service.DebugUtil;
import de.hsos.vs.service.UsernameAlreadyExistsException;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(name = "registerServlet", urlPatterns = {"/register"})
public class RegisterServlet extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {

        String username = req.getParameter("theUser");
        String password = req.getParameter("thePassword");
        String name = req.getParameter("name");
        PrintWriter writer = resp.getWriter();

        // Create a new User object
        User newUser = new User();
        newUser.setUsername(username);
        newUser.setPassword(password);
        newUser.setName(name);

        try {
            // Register the user
            AuthenticationService.registerUser(newUser);
            // Store username in session
            req.getSession().setAttribute("username", username);

            // Redirect to welcome page after successful registration
            writer.println("<font color=green>Register successful! Login to your account now</font>");
            req.getRequestDispatcher("/login.html").include(req, resp);

        } catch (Exception e) {
            // Forward to registration page with error message
            forwardToHomePage(req, resp, "Registration failed: " + e.getMessage());
        }
    }

    private void forwardToHomePage(HttpServletRequest req, HttpServletResponse resp,
                                   String errorMessage)
            throws ServletException, IOException {
        
        PrintWriter out= resp.getWriter();
        out.println("<font color=red>" + errorMessage + "</font>");
        req.getRequestDispatcher("/").include(req, resp);
        DebugUtil.log("Forwarded to homepage");
    }
}
