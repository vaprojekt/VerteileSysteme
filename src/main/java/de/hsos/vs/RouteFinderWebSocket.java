package de.hsos.vs;

import de.hsos.vs.entity.AutoCompleteResult;
import de.hsos.vs.entity.Route;
import de.hsos.vs.service.ApiService;
import de.hsos.vs.service.DebugUtil;
import de.hsos.vs.service.RouteManagerService;
import jakarta.websocket.OnClose;
import jakarta.websocket.OnError;
import jakarta.websocket.OnMessage;
import jakarta.websocket.OnOpen;
import jakarta.websocket.Session;
import jakarta.websocket.server.ServerEndpoint;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/*
 This class will handle WebSocket connections for the route finder application,
 and process route search requests and communicate with the OTP-API.
*/
@ServerEndpoint("/routefinder")
public class RouteFinderWebSocket {
    private static final Map<String, Session> sessions = new ConcurrentHashMap<>();
    private Map<String, Long> lastRequestTimestamps = new ConcurrentHashMap<>();
    private static final long THROTTLE_INTERVAL = 1000; // Throttle interval in milliseconds (adjust as needed)


    @OnOpen
    public void onOpen(Session session) {
        sessions.put(session.getId(), session);
        DebugUtil.log("WebSocket opened sessionId: " + session.getId());
    }

    @OnMessage
    public void onMessage(String message, Session session) {
        DebugUtil.log("WebSocket message from sessionId " + session.getId() + ": " + message);

        // Handle incoming messages (e.g., route search requests)
        handleWebSocketMessage(message, session);
    }

    @OnClose
    public void onClose(Session session) {
        sessions.remove(session.getId());
        DebugUtil.log("WebSocket closed: sessionId " + session.getId());
    }

    @OnError
    public void onError(Throwable t) {
        DebugUtil.log("Error: " + t.getMessage());
    }

    private void handleWebSocketMessage(String message, Session session) {
        try {
            JSONObject jsonMessage = new JSONObject(message);

            // Extract the action type from the message
            String action = jsonMessage.getString("action");

            if (action.endsWith("Autocomplete")) {
                handleAutocomplete(jsonMessage, session);
            } else if ("searchRoutes".equals(action)) {
                searchRoutes(jsonMessage, session);
            } else if ("addRouteToFavorite".equals(action)) {
                addRouteToFavorite(jsonMessage, session);
            } else if ("removeRouteFromFavorite".equals(action)) {
                removeRouteFromFavorite(jsonMessage, session);
            } else if ("getFavoriteRoutes".equals(action)) {
                getFavoriteRoutes(jsonMessage, session);
            } else {
                DebugUtil.log("Unknown action: " + action);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void getFavoriteRoutes(JSONObject jsonMessage, Session session) {
        try {
            // We get client message from websocket
            String username = jsonMessage.getString("user");

            // Call RouteManagerService to get favorite routes
            List<Route> favoriteRoutesResponse = RouteManagerService.getFavoriteRoutes(username);

            // Build and send a success response to the client
            String successResponse = buildFavoriteRoutesResponse(favoriteRoutesResponse);
            sendWebSocketMessage(session, successResponse);

        } catch (Exception e) {
            // Build and send an error response to the client
            String errorResponse = buildRouteManagerResponse(false, "Error when getting favorite route from DB: " + e.getMessage(), "getFavoriteRoutes");
            sendWebSocketMessage(session, errorResponse);
        }
    }

    private String buildFavoriteRoutesResponse(List<Route> favoriteRoutesResponse) {
        // Build a JSON response based on the list of favorite routes
        JSONObject jsonResponse = new JSONObject();
        jsonResponse.put("action", "getFavoriteRoutes");

        if (favoriteRoutesResponse.isEmpty()) {
            jsonResponse.put("success", false);
            jsonResponse.put("message", "No favorite routes found.");
        } else {
            jsonResponse.put("success", true);
            jsonResponse.put("message", "Favorite routes retrieved successfully");

            // Convert the list of Route objects to a JSON array
            JSONArray routesArray = new JSONArray();
            for (Route route : favoriteRoutesResponse) {
                JSONObject routeJson = new JSONObject();
                routeJson.put("departure", route.getDeparture());
                routeJson.put("destination", route.getDestination());
                routesArray.put(routeJson);
            }

            jsonResponse.put("favoriteRoutes", routesArray);
        }

        return jsonResponse.toString();
    }

    private void removeRouteFromFavorite(JSONObject jsonMessage, Session session) {
        try {
            String departure = jsonMessage.getString("departure");
            String destination = jsonMessage.getString("destination");
            String username = jsonMessage.getString("user");

            // Call RouteManagerService to remove the route from favorites
            RouteManagerService.deleteRouteFromFavorites(departure, destination, username);

            // Build and send a success response to the client
            String successResponse = buildRouteManagerResponse(true, "Route removed from favorites successfully", "removeRouteFromFavorite");
            sendWebSocketMessage(session, successResponse);

        } catch (Exception e) {
            // Build and send an error response to the client
            String errorResponse = buildRouteManagerResponse(false, "Error removing route from favorites: " + e.getMessage(), "removeRouteFromFavorite");
            sendWebSocketMessage(session, errorResponse);
        }
    }

    private String buildRouteManagerResponse(boolean success, String message, String action) {
        // Build a JSON response based on the result of removing the route from favorites
        JSONObject jsonResponse = new JSONObject();
        jsonResponse.put("action", action);
        jsonResponse.put("success", success);
        jsonResponse.put("message", message);

        return jsonResponse.toString();
    }

    private void addRouteToFavorite(JSONObject jsonMessage, Session session) {
        try {
            String departure = jsonMessage.getString("departure");
            String destination = jsonMessage.getString("destination");
            String username = jsonMessage.getString("user");


            // Call RouteManagerService to add the route to favorites
            boolean isRouteAddedToFav = RouteManagerService.addRouteToFavorites(departure, destination, username);
            // Build and send a success response to the client
            String successResponse = buildRouteManagerResponse(isRouteAddedToFav, "Route added to favorites successfully", "addRouteToFavorite");
            sendWebSocketMessage(session, successResponse);

        } catch (Exception e) {
            // Build and send an error response to the client
            String errorResponse = buildRouteManagerResponse(false, "Error adding route to favorites: " + e.getMessage(), "addRouteToFavorite");
            sendWebSocketMessage(session, errorResponse);

        }

    }

    private void handleAutocomplete(JSONObject jsonMessage, Session session) {
        String sessionId = session.getId();

        // Check if throttling is required
        if (shouldThrottle(sessionId)) {
            sendWebSocketErrorMessage(session, "Throttled request. Please wait before making another request.");
            return;
        }

        // Update the last request timestamp
        updateLastRequestTimestamp(sessionId);

        // Extract the received input from the client's message
        String autocompleteInput = jsonMessage.getString("address");

        // Determine whether it's a departure or destination autocomplete
        boolean isDepartureAutocomplete = jsonMessage.getString("action").equals("departureAutocomplete");

        // Call API service to get suggestions based on the input
        List<AutoCompleteResult> autocompleteResults = ApiService.getAutocompleteSuggestions(autocompleteInput);

        // Send the suggestions back to the client
        sendWebSocketMessage(session, buildAutocompleteResponse(autocompleteResults, isDepartureAutocomplete));
    }

    private void searchRoutes(JSONObject jsonMessage, Session session) {
        // Extract relevant information from the message (departure, destination, departureTime)
        String departure = jsonMessage.getString("departureLat") + "," + jsonMessage.getString("departureLng");
        String destination = jsonMessage.getString("destinationLat") + "," + jsonMessage.getString("destinationLng");
        String departureTime = jsonMessage.getString("departureTime");

        try {
            // Call the Api service to fetch data from the API
            String routeResponse = ApiService.fetchRouteData(departure, destination, departureTime);
            //DebugUtil.log(routeResponse);

            // Send the route information to the client via WebSocket
            sendWebSocketMessage(session, buildSearchRouteResponse(routeResponse));
        } catch (Exception e) {
            e.printStackTrace();
            // Handle the exception or send an error message back to the client
            sendWebSocketErrorMessage(session, "Error fetching route data from API");
        }
    }

    private void sendWebSocketErrorMessage(Session session, String errorMessage) {
        try {
            // Construct a JSON object with the error message
            JSONObject errorJson = new JSONObject();
            errorJson.put("error", errorMessage);

            // Send the error message to the client via WebSocket
            session.getBasicRemote().sendText(errorJson.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void sendWebSocketMessage(Session session, String message) {
        try {
            session.getBasicRemote().sendText(message);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private boolean shouldThrottle(String sessionId) {
        long currentTime = System.currentTimeMillis();
        if (lastRequestTimestamps.containsKey(sessionId)) {
            long lastRequestTime = lastRequestTimestamps.get(sessionId);
            return (currentTime - lastRequestTime) < THROTTLE_INTERVAL;
        }
        return false;
    }

    private void updateLastRequestTimestamp(String sessionId) {
        lastRequestTimestamps.put(sessionId, System.currentTimeMillis());
    }

    private String buildAutocompleteResponse(List<AutoCompleteResult> autocompleteResults, boolean isDepartureAutocomplete) {
        // Build a JSON response based on the autocomplete results
        JSONObject jsonResponse = new JSONObject();
        JSONArray suggestionsArray = new JSONArray();

        for (AutoCompleteResult result : autocompleteResults) {
            JSONObject suggestion = new JSONObject();
            suggestion.put("lon", result.getLon());
            suggestion.put("lat", result.getLat());
            suggestion.put("label", result.getLabel());
            suggestionsArray.put(suggestion);
        }

        // Determine the autocomplete action based on the input field
        String autocompleteAction = isDepartureAutocomplete ? "departureAutocomplete" : "destinationAutocomplete";

        jsonResponse.put("action", autocompleteAction);
        jsonResponse.put("suggestions", suggestionsArray);

        return jsonResponse.toString();
    }

    private String buildSearchRouteResponse(String routeResponse) {
        JSONObject jsonResponse = new JSONObject();

        try {
            // Check if the routeResponse is a valid JSON string
            if (isValidJson(routeResponse)) {
                // Parse the routeResponse to extract relevant information
                JSONObject routeData = new JSONObject(routeResponse);

                // Check if there's an error in the response
                if (routeData.has("error")) {
                    JSONObject error = routeData.getJSONObject("error");
                    jsonResponse.put("action", "searchRoutes");
                    jsonResponse.put("error", error);
                    DebugUtil.log("jsonResponse " + jsonResponse);
                } else {
                    // Logic to extract other data from routeResponse
                    jsonResponse.put("action", "searchRoutes");
                    // Add other data to jsonResponse as needed
                    if (routeData.has("plan")) {
                        JSONObject plan = routeData.getJSONObject("plan");
                        jsonResponse.put("plan", plan);
                    }
                    DebugUtil.log("jsonResponse " + jsonResponse);
                }
            } else {
                // Handle the case where routeResponse is not a valid JSON string
                jsonResponse.put("action", "searchRoutes");
                jsonResponse.put("exception", "Invalid JSON format in routeResponse");
            }
        } catch (Exception e) {
            e.printStackTrace();
            // Handle JSON parsing exception
            // Send the exception message to the client
            jsonResponse.put("action", "searchRoutes");
            jsonResponse.put("exception", e.getMessage());
        }

        return jsonResponse.toString();
    }

    private boolean isValidJson(String jsonString) {
        try {
            new JSONObject(jsonString);
            return true;
        } catch (Exception e) {
            return false;
        }
    }


}
