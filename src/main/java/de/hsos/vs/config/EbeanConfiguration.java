package de.hsos.vs.config;

import de.hsos.vs.service.DebugUtil;
import io.ebean.DB;
import io.ebean.Database;
import io.ebean.DatabaseFactory;
import io.ebean.config.DatabaseConfig;
import io.ebean.datasource.DataSourceConfig;

import jakarta.servlet.ServletContextEvent;
import jakarta.servlet.ServletContextListener;
import jakarta.servlet.annotation.WebListener;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Properties;

/**
 * In a Jakarta EE (Java EE) web application, we use a ServletContextListener to execute
 * code during application startup and cleanup.
 */
@WebListener
public class EbeanConfiguration implements ServletContextListener {

    private static Database database;

    @Override
    public void contextInitialized(ServletContextEvent sce) {
        initialize();
        checkDatabaseTables();
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        if (database != null) {
            DebugUtil.log("Shutting down database");
            database.shutdown();
        }
    }

    private static void initialize() {
        if (database == null) {
            // Datasource configuration
            DatabaseConfig config = new DatabaseConfig();

            Properties properties = new Properties();
            properties.put("ebean.db.ddl.generate", "true");
            properties.put("ebean.db.ddl.run", "true");
            properties.put("datasource.db.username", "admin");
            properties.put("datasource.db.password", "admin");
            properties.put("datasource.db.databaseUrl", "jdbc:sqlite:Fahrplaner");
            properties.put("datasource.db.databaseDriver", "org.sqlite.JDBC");
            config.loadFromProperties(properties);
            config.setName("Fahrplaner");
            // Create the database instance
            database = DatabaseFactory.create(config);

            // Perform additional configuration if needed
            // ...

            // Log information about the database connection
            logDatabaseInfo();
        }
    }

    private static void logDatabaseInfo() {
        // Log information about the database (e.g., connection details)
        DebugUtil.log("Connected to database: " + database.name());
    }

    private static void checkDatabaseTables() {
        try (Connection connection = database.dataSource().getConnection()) {
            DatabaseMetaData metaData = connection.getMetaData();

            ResultSet tablesResultSet = metaData.getTables(null, null, null, new String[]{"TABLE"});
            while (tablesResultSet.next()) {
                String tableName = tablesResultSet.getString("TABLE_NAME");
                DebugUtil.log("Table: " + tableName);

                ResultSet columnsResultSet = metaData.getColumns(null, null, tableName, null);
                while (columnsResultSet.next()) {
                    String columnName = columnsResultSet.getString("COLUMN_NAME");
                    DebugUtil.log("Column: " + columnName);
                }
            }
        } catch (SQLException e) {
            DebugUtil.log("Error checking database tables: " + e.getMessage());
        }
    }

}
