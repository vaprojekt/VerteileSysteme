package de.hsos.vs.dao;

import io.ebean.Finder;
import de.hsos.vs.entity.Route;

public class RouteDAO {

    private static final Finder<Long, Route> finder = new Finder<>(Route.class);

    public static Route findRoute(String departure, String destination) {
        return finder.query()
                .where()
                .eq("departure", departure)
                .eq("destination", destination)
                .findOne();
    }

    // You can add other methods as needed for your application
}
