package de.hsos.vs.dao;

import io.ebean.Finder;
import de.hsos.vs.entity.User;

public class UserDAO {

    private static final Finder<Long, User> finder = new Finder<>(User.class);

    public static User findByUsername(String username) {
        return finder.query().where().eq("username", username).findOne();
    }

    public static void saveUser(User user) {
        user.save();
    }

    // Add other methods as needed
}
