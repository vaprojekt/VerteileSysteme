package de.hsos.vs.dao;

import de.hsos.vs.entity.UserRoute;
import de.hsos.vs.entity.User;
import de.hsos.vs.entity.Route;
import io.ebean.Finder;

import java.util.List;

public class UserRouteDAO {
    private static final Finder<Long, UserRoute> finder = new Finder<>(UserRoute.class);

    public static UserRoute findUserRoute(User user, Route route) {
        // Find UserRoute based on User and Route
        return finder.query()
                .where()
                .eq("user", user)
                .eq("route", route)
                .findOne();
    }


    public static List<UserRoute> findUserFavorites(String username) {
        // Find all favorite routes of a specific user based on their username
        return finder.query()
                .fetch("route")  // Eagerly fetch the associated Route entity
                .where()
                .eq("user.username", username)
                .findList();
    }

    public static void saveUserRoute(UserRoute userRoute) {
        userRoute.save();
    }

    public static boolean deleteUserRoute(UserRoute userRoute) {
        return userRoute.delete();
    }
    // Add other methods as needed

}
