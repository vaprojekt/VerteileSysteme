package de.hsos.vs.entity;

public class AutoCompleteResult {
    private double lon;
    private double lat;
    private String label;

    // Constructors, getters, and setters

    public AutoCompleteResult(double lon, double lat, String label) {
        this.lon = lon;
        this.lat = lat;
        this.label = label;
    }

    public double getLon() {
        return lon;
    }

    public void setLon(double lon) {
        this.lon = lon;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }
}
