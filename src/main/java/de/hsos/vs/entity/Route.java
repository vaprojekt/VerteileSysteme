package de.hsos.vs.entity;
import javax.persistence.Entity;
import javax.persistence.Id;
import io.ebean.Model;
import io.ebean.annotation.NotNull;


@Entity(name = "route")
public class Route extends Model {
    @Id
    private int routeID;
    @NotNull
    private String departure;
    @NotNull
    private String destination;

    // Other attributes, constructors, and methods

    public String getDeparture() {
        return departure;
    }

    public void setDeparture(String departure) {
        this.departure = departure;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    // Getters and setters for other attributes

}
