package de.hsos.vs.entity;

import javax.persistence.Entity;
import javax.persistence.Id;
import io.ebean.Model;
import io.ebean.annotation.NotNull;


@Entity(name = "user")
public class User extends Model {
    @Id
    private int userId;
    @NotNull
    private String name;
    @NotNull
    private String username;
    @NotNull
    private String password;

    // Other attributes, constructors, and methods

    /*
    In Ebean we typically don't need to explicitly declare a constructor for our model classes.
    Ebean relies on the default constructor and setters to instantiate and populate the objects.
    */

    public int getUserID() {
        return userId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}