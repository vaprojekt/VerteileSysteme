package de.hsos.vs.entity;
import io.ebean.Model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity(name = "user_route")
public class UserRoute extends Model {
    @Id
    private int id;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;

    @ManyToOne
    @JoinColumn(name = "route_id")
    private Route route;

    // Other attributes, constructors, and methods
    public void setUser(User user) {
        this.user = user;
    }

    public void setRoute(Route newRoute) {
        this.route = newRoute;
    }

    public int getId() {
        return id;
    }

    public User getUser() {
        return user;
    }

    public Route getRoute() {
        return route;
    }
}