package de.hsos.vs.service;

import de.hsos.vs.entity.AutoCompleteResult;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONException;

import java.net.URI;
import java.net.URISyntaxException;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.net.http.HttpClient;
import java.io.IOException;
import java.util.*;

public class ApiService {

    private static final String VBN_API_BASE_URL = "http://gtfsr.vbn.de/api/routers/connect/plan";
    private static final String VBN_API_KEY = "6e915416754.....";
    private static final String ORS_API_BASE_URL = "https://api.openrouteservice.org/geocode/autocomplete";
    private static final String ORS_API_KEY = "5b3ce3597851110001cf6248935....";

    public static String fetchRouteData(String departure, String destination, String departureTime) throws IOException, InterruptedException, URISyntaxException {

        HttpResponse<String> response = null;
        String responseBody = "";
        String formattedDate = DateTimeConverter.getFormattedDate(departureTime);
        String formattedTime = DateTimeConverter.getFormattedTime(departureTime);

        try {
        // Construct the URL with query parameters
        URI uri = URI.create(VBN_API_BASE_URL +
                "?arriveBy=false" +
                "&date=" + formattedDate +
                "&fromPlace=" + departure +
                "&toPlace=" + destination +
                "&time=" + formattedTime +
                "&mode=WALK,TRANSIT" +
                "&maxWalkDistance=800");

        // Create the HTTP client
        HttpClient httpClient = HttpClient.newHttpClient();

        // Build the HTTP request
        HttpRequest request = HttpRequest.newBuilder()
                .uri(uri)
                .header("Authorization", VBN_API_KEY)
                .method("GET", HttpRequest.BodyPublishers.noBody())
                .build();

            // Send the HTTP request and get the response
            response = httpClient.send(request, HttpResponse.BodyHandlers.ofString());
        } catch (Exception e) {
            e.printStackTrace();
        }

        if(response!=null) {
            // Get the response body
            responseBody = response.body();

            //DebugUtil.log(responseBody);
        } else {
            DebugUtil.log("Something wrong! Cannot get response");
        }

        return responseBody;
    }

    public static List<AutoCompleteResult> getAutocompleteSuggestions(String input) {
        // Space in input text not allowed
        URI uri = URI.create(ORS_API_BASE_URL +
                "?api_key=" + ORS_API_KEY +
                "&text=" + input.replace(" ", "%20")
        );

        // Create the HTTP client
        HttpClient httpClient = HttpClient.newHttpClient();
        // Build the HTTP request
        HttpRequest request = HttpRequest.newBuilder()
                .uri(uri)
                .header("Accept", "application/json, application/geo+json, application/gpx+xml, img/png; charset=utf-8")
                .method("GET", HttpRequest.BodyPublishers.noBody())
                .build();

        try {
            // Send the HTTP request and get the response
            HttpResponse<String> response = httpClient.send(request, HttpResponse.BodyHandlers.ofString());
            //DebugUtil.log(response.body());
            // Parse the JSON response and extract autocomplete suggestions
            List<AutoCompleteResult> autocompleteResults = parseAutocompleteResponse(response.body());

            return autocompleteResults;
        } catch (Exception e) {
            e.printStackTrace();
            return Collections.emptyList();
        }
    }

    private static List<AutoCompleteResult> parseAutocompleteResponse(String response) {
        List<AutoCompleteResult> autocompleteResults = new ArrayList<>();

        try {
            JSONObject jsonResponse = new JSONObject(response);
            JSONArray featuresArray = jsonResponse.getJSONArray("features");

            for (int i = 0; i < featuresArray.length(); i++) {
                JSONObject feature = featuresArray.getJSONObject(i);
                JSONObject geometry = feature.getJSONObject("geometry");
                JSONArray coordinates = geometry.getJSONArray("coordinates");

                double lon = coordinates.getDouble(0);
                double lat = coordinates.getDouble(1);
                String label = feature.getJSONObject("properties").getString("label");

                AutoCompleteResult result = new AutoCompleteResult(lon, lat, label);
                autocompleteResults.add(result);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return autocompleteResults;
    }



}
