package de.hsos.vs.service;

import de.hsos.vs.config.EbeanConfiguration;
import de.hsos.vs.dao.UserDAO;
import de.hsos.vs.entity.User;
import io.ebean.Database;

public class AuthenticationService {

    public static boolean authenticateUser(String username, String password) {
        DebugUtil.log("Authenticating user: " + username);

        User user = UserDAO.findByUsername(username);
        return user != null && user.getPassword().equals(password);
    }

    public static void registerUser(User user) throws UsernameAlreadyExistsException {
        DebugUtil.log("Registering user: " + user.getUsername());

        User existingUser = UserDAO.findByUsername(user.getUsername());
        if (existingUser == null) {
            // Save the new user to the database
            UserDAO.saveUser(user);
        } else {
            // Handle username already exists error
            throw new UsernameAlreadyExistsException("Username already exists. Please choose a different username.");
        }
    }

    // Add other methods as needed
}
