package de.hsos.vs.service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.Locale;

public class DateTimeConverter {

    // Convert departureTime to API-compatible format
    public static String convertToApiFormat(String departureTime) {
        // That the incoming format is in ISO-8601
        LocalDateTime dateTime = LocalDateTime.parse(departureTime);
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MM-dd-yyyy HH:mm");
        return dateTime.format(formatter);
    }

    // Get formatted date
    public static String getFormattedDate(String dateTime) {
        return convertToApiFormat(dateTime).split(" ")[0];
    }

    // Get formatted time
    public static String getFormattedTime(String dateTime) {
        return convertToApiFormat(dateTime).split(" ")[1];
    }

    public static Date fromTimestamp(String value) {
        if (value == null) {
            return null;
        } else {
            try {
                return new SimpleDateFormat("MM dd, yyyy", Locale.getDefault()).parse(value);
            } catch (ParseException e) {
                e.printStackTrace();
                return null;
            }
        }
    }

    public static String dateToTimestamp(Date date) {
        return date != null ? new SimpleDateFormat("MM dd, yyyy", Locale.getDefault()).format(date) : null;
    }

}
