package de.hsos.vs.service;

public class DebugUtil {

    public static void log(String message) {
        StackTraceElement[] stackTraceElements = Thread.currentThread().getStackTrace();
        if (stackTraceElements.length >= 3) {
            StackTraceElement caller = stackTraceElements[2];
            String className = caller.getClassName();
            String methodName = caller.getMethodName();

            System.out.println(className + "." + methodName + ": " + message);
        } else {
            System.out.println("Unknown caller: " + message);
        }
    }
}