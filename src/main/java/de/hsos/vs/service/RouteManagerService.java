package de.hsos.vs.service;

import de.hsos.vs.dao.RouteDAO;
import de.hsos.vs.dao.UserDAO;
import de.hsos.vs.dao.UserRouteDAO;
import de.hsos.vs.entity.Route;
import de.hsos.vs.entity.User;
import de.hsos.vs.entity.UserRoute;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

public class RouteManagerService {

    public static boolean addRouteToFavorites(String departure, String destination, String username) throws Exception {
        try {
            // Find the user by username
            User user = UserDAO.findByUsername(username);

            if (user != null) {
                // Check if the route already exists in favorites
                Route existingRoute = RouteDAO.findRoute(departure, destination);

                if (existingRoute == null) {
                    // If the route doesn't exist, create a new route
                    Route newRoute = new Route();
                    newRoute.setDeparture(departure);
                    newRoute.setDestination(destination);
                    newRoute.save(); // Save the new route to the database

                    // Create a new UserRoute entity to represent the relationship
                    UserRoute userRoute = new UserRoute();
                    userRoute.setUser(user);
                    userRoute.setRoute(newRoute);

                    UserRouteDAO.saveUserRoute(userRoute); // Save the relationship to the database
                    DebugUtil.log("Added userRoute to DB");
                    return true;

                } else {
                    // Check if another user has already added the route to their favorites
                    if (!isRouteInFavorites(existingRoute, user)) {
                        // Create a new UserRoute entity to represent the relationship
                        UserRoute userRoute = new UserRoute();
                        userRoute.setUser(user);
                        userRoute.setRoute(existingRoute);

                        UserRouteDAO.saveUserRoute(userRoute); // Save the relationship to the database
                        DebugUtil.log("isRouteInFavorites? Added userRoute to DB");
                        return true;
                    }
                    // the route existed in favorites of user, can't add twice
                    throw new Exception("The route existed in favorites of user. Can't add twice");
                }
            }
        } catch (Exception e) {
            throw new Exception(e.getMessage());
        }
        return false;
    }

    private static boolean isRouteInFavorites(Route route, User user) {
        // Check if the route is already in the favorites of the given user
        UserRoute userRoute = UserRouteDAO.findUserRoute(user, route);
        return userRoute != null;
    }

    public static void deleteRouteFromFavorites(String departure, String destination, String username) throws Exception {
        try {
            // Find the user by username
            User user = UserDAO.findByUsername(username);

            if (user != null) {
                // Find the route by departure and destination
                Route existingRoute = RouteDAO.findRoute(departure, destination);

                if (existingRoute != null) {
                    // Find and delete the UserRoute entry
                    UserRoute userRoute = UserRouteDAO.findUserRoute(user, existingRoute);

                    if (userRoute != null) {
                        if (UserRouteDAO.deleteUserRoute(userRoute)) {
                            DebugUtil.log("Deleted userRoute from DB");
                        } else {
                            throw new Exception("Error deleting userRoute");
                        }
                    } else {
                        throw new Exception("UserRoute not found for deletion");
                    }
                } else {
                    throw new Exception("Route not found for deletion");
                }
            }
        } catch (Exception e) {
            throw new Exception(e.getMessage());
        }
    }

    public static List<Route> getFavoriteRoutes(String username) throws Exception {
        try {
            // Find the user by username
            User user = UserDAO.findByUsername(username);

            if (user != null) {
                // Find the UserRoute entries for this user
                List<UserRoute> userRouteList = UserRouteDAO.findUserFavorites(user.getUsername());

                if (!userRouteList.isEmpty()) {
                    List<Route> favoriteRoutes = new ArrayList<>();

                    // Iterate through userRouteList and extract Route objects
                    for (UserRoute userRoute : userRouteList) {
                        favoriteRoutes.add(userRoute.getRoute());
                    }

                    return favoriteRoutes;
                } else {
                    // Return an empty list if no favorite routes are found
                    return Collections.emptyList();
                }
            }
        } catch (Exception e) {
            throw new Exception(e.getMessage());
        }

        // Return an empty list if no user is found
        return Collections.emptyList();
    }

}
