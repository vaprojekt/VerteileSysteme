let currentFavoriteJsonResponse;
let currentUserName;

function displaySearchResults(jsonResponse) {
    const error = jsonResponse.error;

    // Check if there is an error in the response
    if (error) {
        const errorMessage = error.msg || 'An error occurred while planning the selected route. Please try again.';
        return `<div class="error-message">${errorMessage}</div>`;
    }
    return routeSummaryViewBuilder(jsonResponse);
}

function routeSummaryViewBuilder(jsonResponse) {
    // Process and display the search results
    const itineraries = jsonResponse.plan.itineraries;
    console.log(itineraries)
    const departureDate = new Date(jsonResponse.plan.date);

    const options = {
        weekday: 'long',
        year: 'numeric',
        month: '2-digit',
        day: '2-digit',
    };

    const formattedDate = departureDate.toLocaleDateString('en-DE', options);
    const from = formattedAddress(selectedDeparture); //jsonResponse.plan.from.name;
    const to = formattedAddress(selectedDestination); //jsonResponse.plan.to.name;

    if (itineraries && itineraries.length > 0) {
        // Build the header row
        let tableHtml = `
            <h4>${formattedDate}</h4>
            <p><strong>From:</strong> ${from}</p>
            <p><strong>To:</strong> ${to}</p>
            <table>
                <thead>
                    <tr>
                        <th>Trip No</th>
                        <th>Departure Time</th>
                        <th>Arrival Time</th>
                        <th>Connections</th>
                        <th>Duration (minutes)</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
        `;

        // Build rows for each itinerary
        itineraries.forEach((itinerary, index) => {
            const departureTime = new Date(itinerary.startTime).toLocaleTimeString();
            const arrivalTime = new Date(itinerary.endTime).toLocaleTimeString();
            const legs = itinerary.legs.length;
            const durationInMinutes = secondToMinutes(itinerary.duration);

            // Build HTML for each itinerary
            tableHtml += `
                <tr>
                    <td>${index + 1}</td>
                    <td>${departureTime}</td>
                    <td>${arrivalTime}</td>
                    <td>${legs}</td>
                    <td>${durationInMinutes}</td>
                    <td><button type="button" onclick="viewRouteDetail(${index})">Detail</button></td>
                </tr>
            `;
        });

        // Close the table
        tableHtml += `
                </tbody>
            </table>
        `;

        return tableHtml;
    } else if (itineraries && itineraries.length === 0) {
        return '<p>No routes found on the selected date time.</p>';
    } else {
        return '<p>No routes found for the selected departure and destination.</p>';
    }
}

function viewRouteDetail(index) {
    const itinerary = selectedRouteJsonResponse.plan.itineraries[index];
    resetPreviousMessage();

    // Check if the itinerary is valid
    if (itinerary) {
        // Create a modal
        const modal = document.createElement('div');
        modal.classList.add('modal-window');
        document.body.appendChild(modal);

        // Build HTML for the modal content (table format)
        const legsHtml = `
            <table>
                <thead>
                    <tr>
                        <th>Connection Section</th>
                        <th>BY</th>
                        <th>Departure Location</th>
                        <th>Departure Time</th>
                        <th>Arrival Location</th>
                        <th>Arrival Time</th>
                        <th>Duration (minutes)</th>
                        <th>Distance (km)</th>
                    </tr>
                </thead>
                <tbody>
                    ${itinerary.legs.map((leg, legIndex) => {
            const modeDetails = leg.mode === 'WALK' ? 'WALK' : `${leg.mode} ${leg.routeShortName} to ${leg.headsign}`;
            // Modify departure and arrival locations if equal to Origin/Destination
            const departureLocation = (leg.from.name === "Origin")
                ? selectedDeparture.split(',')[0].trim() // Use the part before the first comma
                : leg.from.name;

            const arrivalLocation = (leg.to.name === "Destination")
                ? selectedDestination.split(',')[0].trim() // Use the part before the first comma
                : leg.to.name;
            const departureTime = new Date(leg.startTime).toLocaleTimeString('en-US', { hour: 'numeric', minute: 'numeric' });
            const arrivalTime = new Date(leg.endTime).toLocaleTimeString('en-US', { hour: 'numeric', minute: 'numeric' });
            const durationInMinutes = Math.floor(leg.duration / 60);
            const distanceInKm = (leg.distance / 1000).toFixed(2);
            const departureDelay = leg.departureDelay/60;
            const arrivalDelay = leg.arrivalDelay/60;

            // Determine the color for delay (green if 0, red otherwise)
            const departureDelayColor = departureDelay === 0 ? 'green' : 'red';
            const arrivalDelayColor = arrivalDelay === 0 ? 'green' : 'red';

            return `
                            <tr>
                                <td>${legIndex + 1}</td>
                                <td>${modeDetails}</td>
                                <td>${departureLocation}</td>
                                <td>${departureTime} <span style="color: ${departureDelayColor}">(+${departureDelay} min)</span></td>
                                <td>${arrivalLocation}</td>
                                <td>${arrivalTime} <span style="color: ${arrivalDelayColor}">(+${arrivalDelay} min)</span></td>
                                <td>${durationInMinutes}</td>
                                <td>${distanceInKm}</td>
                            </tr>
                            `;
        }).join('')}
                </tbody>
            </table>
        `;

        // Set the modal content
        modal.innerHTML = `
            <div>
                <a href="#" title="Close" onclick="closeModal()" class="modal-close">Close</a>
                <h1>Route Detail</h1>
                <p><strong>Departure:</strong> ${formattedAddress(selectedDeparture)}</p>
                <p><strong>Destination:</strong> ${formattedAddress(selectedDestination)}</p>
                ${legsHtml}
            </div>
        `;

        // Display the modal
        modal.style.display = 'block';
    }
}

function closeModal() {
    const modal = document.querySelector('.modal-window');
    if (modal) {
        modal.style.display = 'none';
        document.body.removeChild(modal);
    }
}

function addToFavorites(username) {
    // Check if a route is selected
    // Validate input and send request to the server
    if (validateInput()) {
        // Create a message object for adding to favorites
        const message = {
            action: "addRouteToFavorite",
            departure: selectedDeparture,
            destination: selectedDestination,
            user: username
        };
        currentUserName = username;
        // Send the WebSocket message to the server
        sendWebSocketMessage(JSON.stringify(message));
    }

}

function openFavoritesModal(username) {
    // Create a message object for getting routes from favorites
    // Send websocket message to server to get favorite routes from DB
    const message = {
        action: "getFavoriteRoutes",
        user: username
    };
    // Send the WebSocket message to the server
    sendWebSocketMessage(JSON.stringify(message));
}

// Get response message from server with corresponding data and display in modal as table
function favoriteListInModalBuilder(jsonResponse) {
    // Reset Previous Error Message
    resetPreviousMessage();
    currentFavoriteJsonResponse = jsonResponse;
    // Process and display the favorite results
    const favoriteRoutes = jsonResponse.favoriteRoutes;
    console.log(favoriteRoutes);
    resetPreviousMessage();

    <!-- Favorites Modal -->
    // Create a modal
    const modal = document.createElement('div');
    modal.classList.add('modal-window');
    document.body.appendChild(modal);
    let routesHtml = "";

    // Check if the favoriteRoutes List is valid
    if (favoriteRoutes && favoriteRoutes.length > 0) {
        // Build HTML for the modal content (table format)
        routesHtml = `
            <table>
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Departure Location</th>
                        <th>Arrival Location</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    ${favoriteRoutes.map((route, routeIndex) => {
            const departureLocation = route.departure;
            const arrivalLocation = route.destination;

            return `
                            <tr>
                                <td>${routeIndex + 1}</td>
                                <td>${departureLocation}</td>
                                <td>${arrivalLocation}</td>
                                <td>
                                    <!-- <button type="button" onclick="LoadRoute(${routeIndex})">Load This Route</button> -->
                                    <button type="button" onclick="deleteRoute(${routeIndex})">Delete This Route</button>
                                </td>
                            </tr>
                            `;
        }).join('')}
                </tbody>
            </table>
        `;

    } else {
        routesHtml = '<p>You have no routes in the favorite list yet.</p>';
    }

    // Set the modal content
    modal.innerHTML = `
            <div>
                <a href="#" title="Close" onclick="closeModal()" class="modal-close">Close</a>
                <h1>Your Favorite Routes</h1>
                ${routesHtml}
            </div>
        `;

    // Display the modal
    modal.style.display = 'block';
}

function deleteRoute(index) {
    // Access the selected route using the index parameter
    const selectedRoute = currentFavoriteJsonResponse.favoriteRoutes[index];

    // Check if the selectedRoute is valid
    if (selectedRoute) {
        // Parse the departure and destination of the route
        const departure = selectedRoute.departure;
        const destination = selectedRoute.destination;

        // Display a confirmation dialog
        const confirmation = window.confirm(`Are you sure you want to delete the route from ${departure} to ${destination}?`);

        // Check user's response
        if (confirmation) {
            // Create a message object for deleting the selected route from favorites
            const message = {
                action: "removeRouteFromFavorite",
                user: currentUserName,
                departure: departure,
                destination: destination
            };

            // Send the WebSocket message to the server
            sendWebSocketMessage(JSON.stringify(message));

            // Close the modal
            closeModal();
        } else {
            // User clicked 'Cancel' in the confirmation dialog
            console.log('Deletion canceled by user.');
        }
    }
}
