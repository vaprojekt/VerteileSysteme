let websocket;
let destinationInput;
let departureInput;
let departureLat;
let departureLng;
let destinationLat;
let destinationLng;
let selectedDeparture;
let selectedDestination;
let selectedRouteJsonResponse;
let selectedDepartureTime;
function $(id) {
    return document.getElementById(id);
}

function getContextPath() {
    return window.location.pathname.substring(0, window.location.pathname.indexOf("/",2));
}

function initWebSocket() {
    const url = "ws://" + location.host + getContextPath() + "/routefinder";
    websocket = new WebSocket(url);

    websocket.onopen = function () {
        console.log("WebSocket opened");
    };

    websocket.onmessage = function (event) {
        const responseText = event.data;
        const jsonResponse = JSON.parse(responseText);
        //console.log("responseText:", jsonResponse);
        const action = jsonResponse.action;

        if (action === "departureAutocomplete") {
            updateDatalist($('departureList'), jsonResponse.suggestions);
        } else if (action === "destinationAutocomplete") {
            updateDatalist($('destinationList'), jsonResponse.suggestions);
        } else if (action === "searchRoutes") {
            if (jsonResponse.exception) {
                // Handle the exception, e.g., display an error message
                $('searchResults').innerHTML = `<p>Error: ${jsonResponse.exception}</p>`;
            } else {
                $('searchResults').innerHTML = displaySearchResults(jsonResponse);
                // Additional logic to update the UI based on real-time data
                selectedRouteJsonResponse = jsonResponse;
            }
        } else if (action === "addRouteToFavorite") {
            if (!jsonResponse.success) {
                // Handle the case adding route un success
                $('addToFavoritesError').innerHTML = `<p>${jsonResponse.message}</p>`;
            } else {
                $('addToFavoritesSuccess').innerHTML = `<p>${jsonResponse.message}</p>`;
            }
        } else if (action === "getFavoriteRoutes") {
            if (!jsonResponse.success) {
                // Handle the case getting route un success
                $('getFavoriteRoutesError').innerHTML = `<p>${jsonResponse.message}</p>`;
            } else {
                favoriteListInModalBuilder(jsonResponse);
            }
        } else if (action === "removeRouteFromFavorite") {
            if (!jsonResponse.success) {
                // Handle the case removing route un success
                $('getFavoriteRoutesError').innerHTML = `<p>${jsonResponse.message}</p>`;
            } else {
                $('getFavoriteRoutesError').innerHTML = `<p class="success-message">${jsonResponse.message}</p>`;
            }
        } /*else {
            console.error("Unknown action:", action);
        }*/

    };

    websocket.onerror = function (event) {
        console.error("WebSocket error:", event);
    };
}

function sendWebSocketMessage(message) {
    if (websocket && websocket.readyState === WebSocket.OPEN) {
        websocket.send(message);
    }
}

function searchRoutes() {
    // Validate input and send request to the server
    if (validateInput(selectedDeparture, selectedDestination, selectedDepartureTime)) {
        // Modify the message object to include latitude and longitude
        const message = {
            action: "searchRoutes",
            departure: selectedDeparture,
            destination: selectedDestination,
            departureTime: selectedDepartureTime,
            departureLat: departureLat,
            departureLng: departureLng,
            destinationLat: destinationLat,
            destinationLng: destinationLng
        };
        sendWebSocketMessage(JSON.stringify(message));
    }
}

function initInputListener() {
    departureInput = $('departure');
    destinationInput = $('destination');

    // Add event listeners after obtaining the input elements
    departureInput.addEventListener('input', (event) => handleInput(event, 'departure'));
    destinationInput.addEventListener('input', (event) => handleInput(event, 'destination'));
}

function handleInput(event, field) {
    // Get the current input value
    let inputValue = event.target.value;

    const message = {
        action: `${field}Autocomplete`,
        address: inputValue
    };

    // Send the input value to the server via WebSocket
    sendWebSocketMessage(JSON.stringify(message));

    // Add event listener for input changes (including selecting from datalist)
    event.target.addEventListener('input', function () {
        // Get the associated datalist
        const datalist = $(`${field}List`);
        const options = datalist.options;

        //console.log("On handleInput, options: ", options);

        const selectedOption = findOption(options, event.target.value);
        //console.log("On handleInput, selectedOption: ", selectedOption);

        const selectedPlace = {
            label: event.target.value,
            lat: selectedOption ? selectedOption.getAttribute('data-lat') : null,
            lon: selectedOption ? selectedOption.getAttribute('data-lon') : null
        };

        //console.log("On handleInput, selectedPlace ", selectedPlace);
        handleSelection(selectedPlace, field);
    });
}

function findOption(options, value) {
    for (let i = 0; i < options.length; i++) {
        if (options[i].value === value) {
            return options[i];
        }
    }
    return null;
}


// Function to update Autocomplete based on server response
function updateDatalist(datalist, suggestions) {
    // Clear existing suggestions
    datalist.innerHTML = '';

    // Populate datalist element with suggestions
    suggestions.forEach((suggestion) => {
        const option = document.createElement('option');
        option.value = suggestion.label;
        option.setAttribute('data-lat', suggestion.lat);
        option.setAttribute('data-lon', suggestion.lon);

        // Populate datalist
        datalist.appendChild(option); //option.cloneNode(true)
    });
    //console.log("On updateDatalist, datalist", datalist);
}

// Function to handle selection from suggestion list
function handleSelection(selectedPlace, field) {
    const infoDiv = $(`${field}Info`);
    infoDiv.innerHTML = `You have selected: ${selectedPlace.label}<br>Lat: ${selectedPlace.lat}<br>Lon: ${selectedPlace.lon}`;

    // Save the latitude and longitude based on the field
    if (field === 'departure') {
        departureLat = selectedPlace.lat;
        departureLng = selectedPlace.lon;
    } else if (field === 'destination') {
        destinationLat = selectedPlace.lat;
        destinationLng = selectedPlace.lon;
    }
}

function resetPreviousMessage() {
    // Reset previous error messages
    $("departureError").textContent = '';
    $("destinationError").textContent = '';
    $("departureTimeError").textContent = '';
    $("addToFavoritesError").textContent = '';
    $("addToFavoritesSuccess").textContent = '';
    $("getFavoriteRoutesError").textContent = '';
}

function validateInput() {
    selectedDeparture = departureInput.value;
    selectedDestination = destinationInput.value;
    selectedDepartureTime= $('departureTime').value;

    console.log("Selected Departure Time:", selectedDepartureTime);
    console.log("Selected Departure:", selectedDeparture, "Lat:", departureLat, "Lng:", departureLng);
    console.log("Selected Destination:", selectedDestination, "Lat:", destinationLat, "Lng:", destinationLng);

    resetPreviousMessage();

    let isValid = true;

    if (!selectedDeparture.trim()) {
        $('departureError').textContent = 'Departure field must not be empty';
        isValid = false;
    } else if (departureLat === null || departureLng === null) {
        $('departureError').textContent = 'Invalid departure location. Please select a valid place from the suggestions.';
        isValid = false;
    }

    if (!selectedDestination.trim()) {
        $('destinationError').textContent = 'Destination field must not be empty';
        isValid = false;
    } else if (destinationLat === null || destinationLng === null) {
        $('destinationError').textContent = 'Invalid destination location. Please select a valid place from the suggestions.';
        isValid = false;
    }

    if (!selectedDepartureTime.trim()) {
        $('departureTimeError').textContent = 'Departure Time field must not be empty';
        isValid = false;
    }

    return isValid;
}

function formattedAddress(place) {
    const commaIndex = place.indexOf(',');
    if (commaIndex !== -1) {
        const secondCommaIndex = place.indexOf(',', commaIndex + 1);
        if (secondCommaIndex !== -1) {
            return place.substring(0, secondCommaIndex).trim();
        }
    }
    return place;
}


function secondToMinutes(second) {
    return Math.floor(second / 60);
}

