<%@ page import="org.joda.time.DateTime" %>
<%@ page language="java" contentType="text/html; charset=US-ASCII"
         pageEncoding="US-ASCII"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=US-ASCII">
    <title>Route Finder</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <script src="https://polyfill.io/v3/polyfill.min.js?features=default"></script>
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,500" rel="stylesheet"/>
<%--
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-rbsA2VBKQhggwzxH7pPCaAqO46MgnOM80zW1RWuH61DGLwZJEdK2Kadq2F9CUG65" crossorigin="anonymous">
    --%>
    <!-- Reference to the external style.css file -->
    <link rel="stylesheet" type="text/css" href="css/style.css">
    <script type="text/javascript" src="js/routefinder.js"></script>
    <script type="text/javascript" src="js/handlerClickOnButtons.js"></script>
</head>
<body>
<%
    //allow access only if session exists
    String user = null;
    DateTime time = DateTime.now();
    if(session.getAttribute("user") == null){
        response.sendRedirect("/");
    }else user = (String) session.getAttribute("user");
    String userName = null;
    String sessionID = null;
    Cookie[] cookies = request.getCookies();
    if(cookies !=null){
        for(Cookie cookie : cookies){
            if(cookie.getName().equals("user")) userName = cookie.getValue();
            if(cookie.getName().equals("JSESSIONID")) sessionID = cookie.getValue();
        }
    }else{
        sessionID = session.getId();
    }
%>
<h3>Hi <%=userName %>, <br>Login successful. <br>Your Session ID=<%=sessionID %></h3>
User: <%=user %>
<br>
Time: <%=time %>
<br>

<h1>Route Finder</h1>
<h3>Search for Routes:</h3>
<form id="routeForm">
    <label class="nameField" for="departure">Departure:</label>
    <input class="inputField" type="text" id="departure" name="departure" list="departureList" required>
    <div id="departureError" class="error-message"></div>
    <!-- Datalist for Departure -->
    <datalist id="departureList"></datalist>
    <!-- display selected place information -->
    <div id="departureInfo"></div>


    <label class="nameField" for="destination">Destination:</label>
    <input class="inputField" type="text" id="destination" name="destination" list="destinationList" required>
    <div id="destinationError" class="error-message"></div>
    <!-- Datalist for Destination -->
    <datalist id="destinationList"></datalist>
    <!-- display selected place information -->
    <div id="destinationInfo"></div>

    <label class="nameField" for="departureTime">Departure Time:</label>
    <input class="inputField" type="datetime-local" id="departureTime" name="departureTime" placeholder="YYYY-MM-DD HH:mm" required>
    <div id="departureTimeError" class="error-message"></div>

    <button type="button" onclick="searchRoutes()">Search</button>
</form>

<h3>Search Results</h3>
<div id="searchResults"></div>

<br>
<!-- Add to Favorites button -->
<button type="button" onclick="addToFavorites('<%=userName %>')">Add Selected Route to Favorites</button>
<div id="addToFavoritesError" class="error-message"></div>
<div id="addToFavoritesSuccess" class="success-message"></div>

<!-- Manager Favorites button -->
<button type="button" onclick="openFavoritesModal('<%=userName %>')">Manage Favorites</button>
<div id="getFavoriteRoutesError" class="error-message"></div>

<!-- need to encode all the URLs where we want session information to be passed -->
<%--<a href="<%=response.encodeURL("CheckoutPage.jsp") %>">Checkout Page</a>--%>
<form action="<%=response.encodeURL("/logout") %>" method="post">
    <input type="submit" value="Logout" >
</form>

<!-- Add this script tag to initialize the WebSocket connection -->
<script type="text/javascript">
    window.onload = function() {
        initWebSocket();
        initInputListener();
    };
</script>


<%--
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-kenU1KFdBIe4zVF0s0G1M5b4hcpxyD9F7jL+jjXkk+Q2h455rYXK/7HAuoJl+0I4" crossorigin="anonymous"></script>
--%>

</body>
</html>
